echo "Coping current data into data.old folder"
cp -r /root/mlops_final_project/infra/processed_data/data /root/mlops_final_project/infra/processed_data/data.old
echo "Coping new data into data folder"
cp -r /root/mlops_final_project/infra/processed_data/generation_2 /root/mlops_final_project/infra/processed_data/data

echo "Updating services"
docker service update --force --update-parallelism 1 --update-delay 30s --update-failure-action rollback mlops_final_project_search_index_0
docker service update --force --update-parallelism 1 --update-delay 30s --update-failure-action rollback mlops_final_project_search_index_1
docker service update --force --update-parallelism 1 --update-delay 30s --update-failure-action rollback mlops_final_project_search_index_2
docker service update --force --update-parallelism 1 --update-delay 30s --update-failure-action rollback mlops_final_project_search_index_3

docker service update --force --update-parallelism 1 --update-delay 30s --update-failure-action rollback mlops_final_project_gateway

echo "Cleaning data.old"
rm -fr /root/mlops_final_project/infra/processed_data/data.old
